package fr.laurentsorba.pojo2avro;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.avro.AvroFactory;
import com.fasterxml.jackson.dataformat.avro.AvroSchema;
import com.fasterxml.jackson.dataformat.avro.schema.AvroSchemaGenerator;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 * Pojo2Avro Loads the avro from the specified inputPojoPackages
 * the generated the avro schema using jackson-dataformat-avro
 * @author Laurent Sorba
 */
@Mojo(name = "pojo2avro", requiresDependencyResolution = ResolutionScope.RUNTIME)
public class Pojo2Avro extends AbstractMojo {

    @Parameter(property = "inputPojoPackages", required = true)
    private List<String> inputPojoListPackages;

    @Parameter(property = "outputNamespacePackage")
    private String outputNamespacePackage;

    @Parameter(property = "outputAvroPath", required = true)
    private String outputAvroPath;

    @Parameter(property = "prettyPrint", defaultValue = "true")
    private String prettyPrint;

    @Parameter(defaultValue = "${project}", readonly = true, required = true)
    private MavenProject project;

    /**
     * Execute method
     * @throws MojoExecutionException can be thrown
     */
    public void execute() throws MojoExecutionException {
        try {
            URLClassLoader classLoader = getUrlClassLoader();

            for (String inputPojoPackage : inputPojoListPackages) {
                getLog().debug("Pojo2Avro.execute: " + inputPojoPackage + " - " + outputAvroPath);

                String outputNsPackage;

                if (outputNamespacePackage == null || outputNamespacePackage.isEmpty()){
                    outputNsPackage = inputPojoPackage;
                } else {
                    outputNsPackage = outputNamespacePackage;
                }

                //Find the POJOs
                String path = inputPojoPackage.replace('.', '/');
                Enumeration<URL> resources = classLoader.getResources(path);
                List<File> dirs = new ArrayList<>();
                while (resources.hasMoreElements()) {
                    URL resource = resources.nextElement();
                    dirs.add(new File(resource.getFile()));
                    getLog().debug("Directory: " + resource.getFile());
                }
                ArrayList<Class> classes = new ArrayList<>();
                for (File directory : dirs) {
                    getLog().debug("Classes from directory: " + directory);
                    try {
                        List<Class> classList = findClasses(directory, inputPojoPackage, classLoader);
                        classes.addAll(classList);
                    } catch (ClassNotFoundException e) {
                        getLog().error(e.getMessage());
                    }
                }

                //Generate the Avro schemas
                for (Class aClass : classes) {
                    getLog().debug("Pojo2Avro.execute.for: " + aClass + " - " + aClass.getSimpleName() + " - " + aClass.getName());
                    try {
                        Path avroPath = Paths.get(outputAvroPath);
                        if (Files.notExists(avroPath)) {
                            getLog().info("Creating folders " + outputAvroPath);
                            Files.createDirectories(avroPath);
                        }

                        String json = generateSchema(aClass, inputPojoPackage, outputNsPackage);
                        Path file = Paths.get(outputAvroPath + aClass.getSimpleName() + ".avsc");

                        getLog().info("Pojo2Avro.execute.for.write: " + file.getFileName());
                        Files.write(file, json.getBytes());
                        //Java11: Files.writeString(file, json, StandardCharsets.UTF_8);
                    } catch (JsonMappingException e) {
                        getLog().error(e.getMessage());
                    } catch (Exception e) {
                        getLog().error(e.getMessage());
                    }
                }
            }
        } catch (IOException e) {
            getLog().error(e.getMessage());
        }

    }

    /**
     * Generate the avro schema out of the POJO
     * @param type class
     * @param inputPojoPackage
     * @param outputNsPackage
     * @return schema
     * @throws JsonMappingException can be thrown
     */
    private String generateSchema(Class<?> type, String inputPojoPackage, String outputNsPackage) throws JsonMappingException {
        ObjectMapper mapper = new ObjectMapper(new AvroFactory());
        AvroSchemaGenerator gen = new AvroSchemaGenerator();
        mapper.acceptJsonFormatVisitor(type, gen);
        AvroSchema schemaWrapper = gen.getGeneratedSchema();
        org.apache.avro.Schema avroSchema = schemaWrapper.getAvroSchema();

        if (outputNsPackage.equals(inputPojoPackage))
            return avroSchema.toString(prettyPrint.equals("true"));

        getLog().info("Replace '"+inputPojoPackage+"' with '"+outputNsPackage+"'");
        return avroSchema.toString(prettyPrint.equals("true"))
                .replace(inputPojoPackage, outputNsPackage);
    }

    /**
     * @return the build path as class loader
     * @throws MalformedURLException can be thrown
     */
    private URLClassLoader getUrlClassLoader() throws MalformedURLException {

        String urlForClassLoader = project.getBuild().getOutputDirectory();
        getLog().info("URL for ClassLoader: " + urlForClassLoader);

        // need to define parent classloader which knows all dependencies of the plugin
        return new URLClassLoader(
                new URL[]{new File(urlForClassLoader).toURI().toURL()},
                Pojo2Avro.class.getClassLoader());
    }

    /**
     * Recursive method used to find all classes in a given directory and subdirs.
     *
     * @param directory   The base directory
     * @param packageName The package name for classes found inside the base directory
     * @return The classes
     * @throws ClassNotFoundException can be thrown
     */
    private static List<Class> findClasses(File directory, String packageName, ClassLoader classLoader) throws ClassNotFoundException {
        List<Class> classes = new ArrayList<>();
        if (!directory.exists()) {
            return classes;
        }
        File[] files = directory.listFiles();
        assert files != null;
        for (File file : files) {
            if (file.isDirectory()) {
                System.out.println("file.isDirectory " + file.getName());
                assert !file.getName().contains(".");
                classes.addAll(findClasses(file, packageName + "." + file.getName(), classLoader));
            } else if (file.getName().endsWith(".class")) {
                Class c = Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6), false, classLoader);
                classes.add(c);
            }
        }
        return classes;
    }
}
