# Pojo2Avro

This is a Maven module to generate Avro schemas from POJOs.
It is using the jackson-dataformat-avro library.

## Command line

To start your application in the dev profile, simply run:

    ./mvnw fr.laurentsorba.pojo2avro:pojo2avro-maven-plugin:1.0-SNAPSHOT:pojo2avro -DinputPojoPackage=com.example.domain -DoutputAvroPath=target/avro

## Maven integration

```
<plugin>
    <groupId>fr.laurentsorba.pojo2avro</groupId>
    <artifactId>pojo2avro-maven-plugin</artifactId>
    <version>${pojo2avro-maven-plugin.version}</version>
    <configuration>
        <inputPojoPackages>
            <param>com.company.domain</param>
        </inputPojoPackages>
        <outputAvroPath>${project.build.directory}/avro/</outputAvroPath>
        <!-- outputNamespacePackage: default: inputPojoPackage -->
        <outputNamespacePackage>com.company.avro</outputNamespacePackage>
        <!-- prettyPrint: default: true -->        
        <prettyPrint>true</prettyPrint>
    </configuration>
    <executions>
        <execution>
            <phase>compile</phase>
            <goals>
                <goal>pojo2avro</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```
